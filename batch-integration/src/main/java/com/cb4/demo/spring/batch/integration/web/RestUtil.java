package com.cb4.demo.spring.batch.integration.web;

import java.io.IOException;
import java.io.Writer;
import java.util.Objects;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;

import com.cb4.demo.spring.batch.integration.core.AppendableWriter;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public enum RestUtil {
    ;

    public static final boolean DEFAULT_PRETTY_PRINT = true;

    public static <M extends ObjectMapper> M configureDefaultMapperProperties(M mapper) {
        if (mapper == null) {
            return mapper;
        }

        mapper.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
        mapper.setVisibility(PropertyAccessor.ALL, Visibility.ANY);
        // Do not allow JSON annotations on fields
        mapper.setVisibility(PropertyAccessor.FIELD, Visibility.NONE);

        return mapper;
    }

    private static final ObjectMapper JSON = configureDefaultMapperProperties(new ObjectMapper());

    public static ObjectMapper getObjectMapper() {
        return JSON;
    }

    public static <O> O sendJsonObject(O entity, HttpServletResponse rsp) throws IOException {
        return sendJsonObject(entity, rsp, DEFAULT_PRETTY_PRINT);
    }

    public static <O> O sendJsonObject(O entity, HttpServletResponse rsp, boolean prettyPrint) throws IOException {
        return sendJsonObject(getObjectMapper(), entity, rsp, prettyPrint);
    }

    public static <O> O sendJsonObject(ObjectMapper mapper, O entity, HttpServletResponse rsp) throws IOException {
        return sendJsonObject(mapper, entity, rsp, DEFAULT_PRETTY_PRINT);
    }

    public static <O> O sendJsonObject(ObjectMapper mapper, O entity, HttpServletResponse rsp, boolean prettyPrint) throws IOException {
        rsp.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        try (Writer writer = rsp.getWriter()) {
            appendJson(mapper, writer, entity, prettyPrint);
        }

        return entity;
    }

    public static <A extends Appendable> A appendJson(A out, Object obj) throws IOException {
        return appendJson(out, obj, false);
    }

    /**
     * Appends the JSON representation of an object to an {@link Appendable} instance
     *
     * @param <A> Type of {@link Appendable} being used
     * @param out The writer output instance
     * @param obj The object to convert - ignored if {@code null}
     * @param prettyPrint Whether to output in pretty-print format
     * @return The updated writer instance (same as input)
     * @throws IOException If failed to convert or write the result
     */
    public static <A extends Appendable> A appendJson(A out, Object obj, boolean prettyPrint) throws IOException {
        return appendJson(getObjectMapper(), out, obj, prettyPrint);
    }

    public static <A extends Appendable> A appendJson(ObjectMapper mapper, A out, Object obj) throws IOException {
        return appendJson(mapper, out, obj, DEFAULT_PRETTY_PRINT);
    }

    public static <A extends Appendable> A appendJson(ObjectMapper mapper, A out, Object obj, boolean prettyPrint) throws IOException {
        Objects.requireNonNull(out, "No writer instance");
        if (obj == null) {
            return out;
        }

        ObjectWriter writer = getObjectWriter(mapper, prettyPrint);
        // Protect original output from closing
        try (Writer w = new AppendableWriter<A>(out) {
            @Override
            public void close() throws IOException {
                // protect original appender from closing
            }
        }) {
            writer.writeValue(w, obj);
        }

        return out;
    }

    public static ObjectWriter getObjectWriter(ObjectMapper mapper, boolean prettyPrint) {
        Objects.requireNonNull(mapper, "No mapper instance");
        return prettyPrint ? mapper.writerWithDefaultPrettyPrinter() : mapper.writer();
    }
}

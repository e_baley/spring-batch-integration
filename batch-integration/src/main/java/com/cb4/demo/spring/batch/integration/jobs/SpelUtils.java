package com.cb4.demo.spring.batch.integration.jobs;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanExpressionContext;
import org.springframework.beans.factory.config.BeanExpressionResolver;
import org.springframework.context.expression.StandardBeanExpressionResolver;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParseException;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.util.ClassUtils;

public enum SpelUtils implements ExpressionParser, BeanExpressionResolver {
    PARSER
    ;

    private final BeanExpressionResolver delegateResolver;
    private final SpelExpressionParser delegateParser;

    SpelUtils() {
        ClassLoader beanClassLoader = ClassUtils.getDefaultClassLoader();
        delegateParser = new SpelExpressionParser(new SpelParserConfiguration(null, beanClassLoader));
        StandardBeanExpressionResolver resolver = new StandardBeanExpressionResolver();
        resolver.setExpressionParser(delegateParser);
        delegateResolver = resolver;
    }

    @Override
    public Object evaluate(String value, BeanExpressionContext evalContext) throws BeansException {
        return delegateResolver.evaluate(value, evalContext);
    }

    @Override
    public Expression parseExpression(String expressionString) throws ParseException {
        return delegateParser.parseExpression(expressionString);
    }

    @Override
    public Expression parseExpression(String expressionString, ParserContext context) throws ParseException {
        return delegateParser.parseExpression(expressionString, context);
    }
}

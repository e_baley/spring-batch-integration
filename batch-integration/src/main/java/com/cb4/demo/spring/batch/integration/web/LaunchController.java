package com.cb4.demo.spring.batch.integration.web;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cb4.demo.spring.batch.integration.core.AbstractLoggingBean;
import com.cb4.demo.spring.batch.integration.jobs.TestJobConfiguration;
import com.cb4.demo.spring.batch.integration.jobs.TestJobRequestsQueue;

@Controller
@RequestMapping("/launch")
public class LaunchController extends AbstractLoggingBean {
    public static final String DEFAULT_TEST_JOB_NAME = "testJob";

    @Inject
    private TestJobRequestsQueue testJobRequestsQueue;

    public LaunchController() {
        super();
    }

    @RequestMapping(path = "/job", method = RequestMethod.PUT)
    public void launchTestJob(HttpServletResponse rsp,
            @RequestParam(name = "name", required = false, defaultValue = DEFAULT_TEST_JOB_NAME) String name,
            @RequestParam(name = "numSteps", required = false, defaultValue = "5") int numSteps)
                    throws IOException {
        name = Validate.notBlank(StringUtils.trimAllWhitespace(name), "No job name specified", ArrayUtils.EMPTY_OBJECT_ARRAY);
        Validate.isTrue(numSteps > 0, "Illegal steps count (%d) for job=%s", numSteps, name);

        TestJobConfiguration jobConfig = new TestJobConfiguration();
        jobConfig.setName(name);
        jobConfig.setNumSteps(numSteps);
        testJobRequestsQueue.postJobLaunchRequest(jobConfig);
        RestUtil.sendJsonObject(jobConfig, rsp);
    }
}

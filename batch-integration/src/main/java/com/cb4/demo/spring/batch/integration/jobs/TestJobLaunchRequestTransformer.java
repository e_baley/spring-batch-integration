package com.cb4.demo.spring.batch.integration.jobs;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.Validate;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.AbstractJob;
import org.springframework.batch.core.job.builder.SimpleJobBuilder;
import org.springframework.batch.integration.launch.JobLaunchRequest;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.cb4.demo.spring.batch.integration.core.AbstractLoggingBean;

@Component
public class TestJobLaunchRequestTransformer extends AbstractLoggingBean {
    public static final long MAX_SLEEP_TIME = 3L;

    @Inject
    private JobBuilderFactory jobBuilderFactory;
    @Inject
    private StepBuilderFactory stepBuilderFactory;
    @Inject
    @Named(IntegrationConfiguration.JOB_NOTIFICATION_EXECUTIONS_LISTENER)
    private JobExecutionListener jobExecutionListener;
    @Inject
    @Named(IntegrationConfiguration.STEP_NOTIFICATION_EXECUTIONS_LISTENER)
    private StepExecutionListener stepExecutionListener;

    public TestJobLaunchRequestTransformer() {
        super();
    }

    @Transformer
    public JobLaunchRequest toRequest(Message<TestJobConfiguration> message) {
        TestJobConfiguration config = message.getPayload();
        logger.info("toRequest - processing request={}", config);

        Job job = createJob(config);
        JobParameters params = new JobParametersBuilder()
                .addString("name", config.getName())
                .addLong("numSteps", Long.valueOf(config.getNumSteps()))
                .addLong("startTime", System.currentTimeMillis())
                .toJobParameters();
        JobLaunchRequest request = new JobLaunchRequest(job, params);
        logger.info("toRequest({}): {}", config, request);
        return request;
    }

    protected Job createJob(TestJobConfiguration config) {
        int numSteps = config.getNumSteps();
        String name = config.getName();
        SimpleJobBuilder jobBuilder = null;
        for (int stepIndex = 1; stepIndex <= numSteps; stepIndex++) {
            Step step = stepBuilderFactory.get(name + "Step" + stepIndex)
                .listener(stepExecutionListener)
                .tasklet(new TestStepTasklet(1L + (stepIndex % MAX_SLEEP_TIME)))
                .build();
            if (stepIndex == 1) {
                jobBuilder = jobBuilderFactory.get(name).start(step);
            } else {
                jobBuilder = jobBuilder.next(step);
            }
        }

        Job job = jobBuilder.build();
        Validate.isInstanceOf(AbstractJob.class, job, "Cannot register job execution listener for %s", job);
        ((AbstractJob) job).registerJobExecutionListener(jobExecutionListener);
        return job;
    }
}

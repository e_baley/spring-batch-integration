package com.cb4.demo.spring.batch.integration.core;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;
import java.util.Objects;

/**
 * Wraps an {@link Appendable} implementation inside a {@link Writer}. <B>Note:</B> if
 * the wrapped instance is {@link Flushable} and/or {@link Closeable} its respective
 * methods will be called.
 *
 * @param <A> Type of wrapped {@link Appendable} instance
 */
public class AppendableWriter<A extends Appendable> extends Writer {
    private final A appender;

    public AppendableWriter(A appender) {
        this.appender = Objects.requireNonNull(appender, "No wrapped appender");
    }

    public A getAppenderInstance() {
        return appender;
    }

    @Override
    public void write(int c) throws IOException {
        append((char) c);
    }

    @Override
    public void write(String str) throws IOException {
        append(str);
    }

    @Override
    public void write(String str, int off, int len) throws IOException {
        append(str, off, off + len);
    }

    @Override
    public Writer append(CharSequence csq) throws IOException {
        Appendable w = getAppenderInstance();
        w.append(csq);
        return this;
    }

    @Override
    public Writer append(CharSequence csq, int start, int end) throws IOException {
        Appendable w = getAppenderInstance();
        w.append(csq, start, end);
        return this;
    }

    @Override
    public Writer append(char c) throws IOException {
        Appendable w = getAppenderInstance();
        w.append(c);
        return this;
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        Appendable w = getAppenderInstance();
        for (int curPos = off, maxPos = off + len; curPos < maxPos; curPos++) {
            w.append(cbuf[curPos]);
        }
    }

    @Override
    public void flush() throws IOException {
        Object w = getAppenderInstance();
        if (w instanceof Flushable) {
            ((Flushable) w).flush();
        }
    }

    @Override
    public void close() throws IOException {
        Object w = getAppenderInstance();
        if (w instanceof Closeable) {
            ((Closeable) w).close();
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[" + getAppenderInstance() + "]";
    }
}

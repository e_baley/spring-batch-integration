package com.cb4.demo.spring.batch.integration.core;

import java.util.Objects;

import org.apache.commons.lang3.Validate;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

public abstract class TypedPayloadLoggingHandler<P> extends LoggingHandler {
    private final Class<P> payloadType;

    protected TypedPayloadLoggingHandler(Class<P> payloadType, String level) {
        super(level);
        this.payloadType = Objects.requireNonNull(payloadType, "No payload type specified");
    }

    protected TypedPayloadLoggingHandler(Class<P> payloadType, Level level) {
        super(level);
        this.payloadType = Objects.requireNonNull(payloadType, "No payload type specified");
    }

    public Class<P> getPayloadType() {
        return payloadType;
    }

    @Override
    protected void handleMessageInternal(Message<?> message) throws Exception {
        super.handleMessageInternal(message);

        Object payload = message.getPayload();
        Class<P> expectedType = getPayloadType();
        Validate.isInstanceOf(expectedType, payload, "Payload not of expected type=%s: %s", expectedType.getSimpleName(), payload);
        handleMessageInternal(expectedType.cast(payload), message.getHeaders());
    }

    protected abstract void handleMessageInternal(P payload, MessageHeaders headers) throws Exception;
}

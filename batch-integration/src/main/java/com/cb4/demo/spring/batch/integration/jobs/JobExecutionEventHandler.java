package com.cb4.demo.spring.batch.integration.jobs;

import org.springframework.batch.core.JobExecution;
import org.springframework.messaging.MessageHeaders;

import com.cb4.demo.spring.batch.integration.core.TypedPayloadLoggingHandler;

public class JobExecutionEventHandler extends TypedPayloadLoggingHandler<JobExecution> {
    public JobExecutionEventHandler(String level) {
        super(JobExecution.class, level);
    }

    public JobExecutionEventHandler(Level level) {
        super(JobExecution.class, level);
    }

    @Override
    protected void handleMessageInternal(JobExecution jobExecution, MessageHeaders headers) throws Exception {
        logger.info("handleMessageInternal(headers=" + headers + "): " + jobExecution);
    }
}

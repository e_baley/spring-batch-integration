package com.cb4.demo.spring.batch.integration.jobs;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import com.cb4.demo.spring.batch.integration.core.AbstractLoggingBean;
import com.cb4.demo.spring.batch.integration.core.CommonAppConfig;

@Component  // TODO see if can make this an InboundChannelAdapter
public class TestJobRequestsQueue extends AbstractLoggingBean {
    public static final String PROP_REQUEST_SEND_TIMEOUT = CommonAppConfig.CONFIG_PROP_PREFIX + ".launch.request.send.timeout";
    public static final long DEFAULT_REQUEST_SEND_TIMEOUT = 5L;

    @Inject
    @Named(IntegrationConfiguration.INBOUND_JOB_REQUEST_CHANNEL)
    private MessageChannel inboundJobRequestChannel;
    @Value("${" + PROP_REQUEST_SEND_TIMEOUT + ":" + DEFAULT_REQUEST_SEND_TIMEOUT + "}")
    private volatile long requestSendTimeout = DEFAULT_REQUEST_SEND_TIMEOUT;

    public TestJobRequestsQueue() {
        super();
    }

    public void postJobLaunchRequest(TestJobConfiguration config) {
        logger.info("postJobLaunchRequest - posting {}", config);

        Message<TestJobConfiguration> message =
            MessageBuilder.withPayload(config)
                .setHeader("requestTime", new Date())
                .build();
        Validate.validState(inboundJobRequestChannel.send(message, TimeUnit.SECONDS.toMillis(requestSendTimeout)),
                "Failed to send request for %s", config);
        logger.info("postJobLaunchRequest - posted {}", config);
    }
}

package com.cb4.demo.spring.batch.integration.jobs;

import java.util.Collections;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.integration.launch.JobLaunchingGateway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.expression.Expression;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.context.IntegrationContextUtils;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.gateway.GatewayCompletableFutureProxyFactoryBean;
import org.springframework.integration.gateway.GatewayMethodMetadata;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.jmx.config.EnableIntegrationMBeanExport;
import org.springframework.integration.transformer.AbstractTransformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.cb4.demo.spring.batch.integration.core.AbstractLoggingBean;
import com.cb4.demo.spring.batch.integration.core.CommonAppConfig;

@Configuration
// see http://docs.spring.io/spring-integration/docs/4.3.9.RELEASE/reference/html/overview.html#configuration-enable-integration
@EnableIntegration
// see http://docs.spring.io/spring-integration/docs/4.3.9.RELEASE/reference/html/system-management-chapter.html#jmx-mbean-exporter
@EnableIntegrationMBeanExport(defaultDomain = CoreConfigBean.MBEAN_DOMAIN_PREFIX, server = "#{" + CoreConfigBean.MBEAN_SERVER + "}")
// see http://docs.spring.io/spring-integration/docs/4.3.9.RELEASE/reference/html/configuration.html#annotations
@IntegrationComponentScan(basePackages = "com.cb4.demo.spring.batch.integration.jobs")
public class IntegrationConfiguration extends AbstractLoggingBean {
    public static final String PROP_LOG_HANDLER_LEVEL = CommonAppConfig.CONFIG_PROP_PREFIX + ".log.handler.level";
    public static final String DEFAULT_LOG_HANDLER_LEVEL = "INFO";  // see LoggingHandler.Level for available values

    public static final String INBOUND_JOB_REQUEST_CHANNEL = "inboundJobRequestChannel";
    public static final String OUTBOUND_JOB_REQUEST_CHANNEL = "outboundJobRequestChannel";
    public static final String JOB_EXECUTION_CHANNEL = "jobExecutionsChannel";
    public static final String JOB_NOTIFICATION_EXECUTIONS_LISTENER = "jobNotificationExecutionsListener";
    public static final String STEP_EXECUTION_CHANNEL = "stepExecutionsChannel";
    public static final String STEP_NOTIFICATION_EXECUTIONS_LISTENER = "stepNotificationExecutionsListener";
    public static final String JOB_LAUNCHING_GATEWAY = "jobLaunchingGateway";

    public static final NavigableMap<String, Expression> DEFAULT_PROXY_HEADERS =
        Collections.unmodifiableNavigableMap(new TreeMap<String, Expression>(String.CASE_INSENSITIVE_ORDER) {
            // Not serializing it
            private static final long serialVersionUID = 1L;

            {
                put("method", SpelUtils.PARSER.parseExpression("#gatewayMethod"));
                put("methodName", SpelUtils.PARSER.parseExpression("#gatewayMethod.name"));
            }
        });

    public IntegrationConfiguration() {
        super();
    }

    @Bean(INBOUND_JOB_REQUEST_CHANNEL)
    public MessageChannel inboundJobRequestChannel() {
        return createMessageChannel(INBOUND_JOB_REQUEST_CHANNEL, DirectChannel::new);
    }

    @Bean(OUTBOUND_JOB_REQUEST_CHANNEL)
    public MessageChannel outboundJobRequestChannel() {
        return createMessageChannel(OUTBOUND_JOB_REQUEST_CHANNEL, DirectChannel::new);
    }

    @Bean(JOB_EXECUTION_CHANNEL)
    public MessageChannel jobExecutionsChannel() {
        return createMessageChannel(JOB_EXECUTION_CHANNEL, DirectChannel::new);
    }

    @Bean(STEP_EXECUTION_CHANNEL)
    public MessageChannel stepExecutionsChannel() {
        return createMessageChannel(STEP_EXECUTION_CHANNEL, DirectChannel::new);
    }

    // see http://docs.spring.io/spring-integration/docs/4.3.9.RELEASE/reference/html/configuration.html#namespace-errorhandler
    // NULL_CHANNEL_BEAN_NAME - see http://docs.spring.io/spring-integration/docs/4.3.9.RELEASE/reference/html/messaging-channels-section.html#channel-special-channels
    @Bean(IntegrationContextUtils.ERROR_CHANNEL_BEAN_NAME)
    public MessageChannel errorChannel() {
        // TODO consider using an executor argument for the PublishSubscribeChannel to make it async...
        return createMessageChannel(IntegrationContextUtils.ERROR_CHANNEL_BEAN_NAME, PublishSubscribeChannel::new);
    }

    // Equivalent to: <int:transformer input-channel="..." output-channel="..." />
    @Bean
    public IntegrationFlow basicIntegrationFlow(TestJobLaunchRequestTransformer requestTransformer) {
        return IntegrationFlows.from(INBOUND_JOB_REQUEST_CHANNEL)
            .transform(new AbstractTransformer() {
                @Override
                protected Object doTransform(Message<?> message) throws Exception {
                    @SuppressWarnings("unchecked")
                    Message<TestJobConfiguration> requestMessage = (Message<TestJobConfiguration>) message;
                    return requestTransformer.toRequest(requestMessage);
                }
            }).channel(OUTBOUND_JOB_REQUEST_CHANNEL)
            .get();
    }

    // Equivalent to <int:gateway service-interface="fqdn of interface" default-request-channel="..." />
    @Bean(JOB_NOTIFICATION_EXECUTIONS_LISTENER)
    public GatewayCompletableFutureProxyFactoryBean jobNotificationExecutionsListener(ConversionService service) {
        return createProxyGateway(JobExecutionListener.class, JOB_EXECUTION_CHANNEL);
    }

    // Equivalent to <int:gateway service-interface="fqdn of interface" default-request-channel="..." />
    @Bean(STEP_NOTIFICATION_EXECUTIONS_LISTENER)
    public GatewayCompletableFutureProxyFactoryBean stepNotificationExecutionsListener(ConversionService service) {
        return createProxyGateway(StepExecutionListener.class, STEP_EXECUTION_CHANNEL);
    }

    protected GatewayCompletableFutureProxyFactoryBean createProxyGateway(Class<?> serviceInterface, String requestChannelName) {
        Validate.isTrue(serviceInterface.isInterface(), "Non-interface to proxy: %s", serviceInterface.getSimpleName());
        Validate.notBlank(requestChannelName, "No request channel specified for proxy of %s", serviceInterface.getSimpleName());

        GatewayCompletableFutureProxyFactoryBean factory =
            new GatewayCompletableFutureProxyFactoryBean(serviceInterface);
        GatewayMethodMetadata globalMetadata = new GatewayMethodMetadata();
        globalMetadata.setHeaderExpressions(DEFAULT_PROXY_HEADERS);
        factory.setGlobalMethodMetadata(globalMetadata);
        factory.setDefaultRequestChannelName(requestChannelName);
        return factory;
    }

    @Bean(JOB_LAUNCHING_GATEWAY)
    public MessageHandler jobLaunchingGateway(JobLauncher jobLauncher) {
        return new JobLaunchingGateway(jobLauncher);
    }

    // Equivalent to <batch-int:job-launching-gateway request-channel="...." reply-channel="...." />
    @Bean
    public IntegrationFlow jobLauncherFlow(@Named(JOB_LAUNCHING_GATEWAY) MessageHandler launcher) {
        /*
         * NOTE: we are using the same channel as the job execution
         * listener. This means that when the job is launched an
         * event message is sent. If we want to distinguish between
         * this event and the before/afterJob proxy calls we need to
         * use a different channel + handler
         */
        return IntegrationFlows.from(OUTBOUND_JOB_REQUEST_CHANNEL)
                .handle(launcher)
                .channel(JOB_EXECUTION_CHANNEL)
                .get();
    }

    @Bean   // equivalent to <int:logging-channel-adapter channel="jobLaunchReplyChannel" level="INFO" />
    public IntegrationFlow jobLaunchingIntegrationFlow(
            @Value("${" + PROP_LOG_HANDLER_LEVEL + ":" + DEFAULT_LOG_HANDLER_LEVEL + "}") String level) {
        return loggingIntegrationFlow(level, JOB_EXECUTION_CHANNEL, JobExecutionEventHandler::new);
    }

    @Bean
    public IntegrationFlow stepLaunchingIntegrationFlow(
            @Value("${" + PROP_LOG_HANDLER_LEVEL + ":" + DEFAULT_LOG_HANDLER_LEVEL + "}") String level) {
        return loggingIntegrationFlow(level, STEP_EXECUTION_CHANNEL, StepExecutionEventHandler::new);
    }

    // equivalent to <int:logging-channel-adapter channel="..." />
    protected IntegrationFlow loggingIntegrationFlow(String level, String inputChannel, Function<String, ? extends LoggingHandler> factory) {
        LoggingHandler handler = factory.apply(level);
        handler.setLoggingEnabled(true);
        handler.setShouldLogFullMessage(true);
        return IntegrationFlows.from(inputChannel)
            .handle(handler)
            .get();
    }

    // see http://docs.spring.io/spring-integration/docs/4.3.9.RELEASE/reference/html/configuration.html#namespace-taskscheduler
    @Bean(IntegrationContextUtils.TASK_SCHEDULER_BEAN_NAME)
    public TaskScheduler integrationTaskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setDaemon(true);
        taskScheduler.setPoolSize(10);
        taskScheduler.setThreadNamePrefix("integration" + StringUtils.capitalize(IntegrationContextUtils.TASK_SCHEDULER_BEAN_NAME));
        return taskScheduler;
    }

    protected <T extends MessageChannel> T createMessageChannel(String name, Supplier<T> creator) {
        T channel = creator.get();
        logger.info("createMessageChannel({}) created {}", name, channel);
        return channel;
    }
}

package com.cb4.demo.spring.batch.integration.jobs;

import java.util.concurrent.TimeUnit;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.cb4.demo.spring.batch.integration.core.AbstractLoggingBean;

public class TestStepTasklet extends AbstractLoggingBean implements Tasklet {
    private final long delaySeconds;

    public TestStepTasklet(long delaySeconds) {
        this.delaySeconds = delaySeconds;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        StepContext stepContext = chunkContext.getStepContext();
        logger.info("execute({}) sleeping {} seconds", stepContext.getStepName(), delaySeconds);
        if (delaySeconds > 0L) {
            Thread.sleep(TimeUnit.SECONDS.toMillis(delaySeconds));
        }
        logger.info("execute({}) finished sleeping {} seconds", stepContext.getStepName(), delaySeconds);
        return RepeatStatus.FINISHED;
    }
}

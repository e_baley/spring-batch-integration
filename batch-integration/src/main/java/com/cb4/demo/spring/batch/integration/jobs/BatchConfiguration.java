package com.cb4.demo.spring.batch.integration.jobs;

import java.util.Collections;
import java.util.NavigableMap;
import java.util.TreeMap;

import javax.inject.Named;
import javax.sql.DataSource;

import org.apache.commons.lang3.Validate;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.support.MapJobRegistry;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.explore.support.JobExplorerFactoryBean;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;

import com.cb4.demo.spring.batch.integration.core.AbstractLoggingBean;
import com.cb4.demo.spring.batch.integration.core.CommonAppConfig;

@Configuration
public class BatchConfiguration extends AbstractLoggingBean {
    public static final String JOB_EXECUTOR_BEAN = "jobExecutor";
    public static final String STEP_EXECUTOR_BEAN = "stepExecuter";
    public static final String JOB_LAUNCHER = "jobLauncher";

    public static final String STEP_EXECUTER_QUEUE_CAPACITY_PROP = CommonAppConfig.CONFIG_PROP_PREFIX + ".step.queue.capacity";
    public static final int DEFAULT_STEP_EXECUTER_QUEUE_CAPACITY = 100;

    public static final NavigableMap<String, String> JDBC2DBTYPE =
        Collections.unmodifiableNavigableMap(new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER) {
            // Not serializing it
            private static final long serialVersionUID = 1L;

            {
                put("postgresql", "POSTGRES");
            }
        });

    public BatchConfiguration() {
        super();
    }

    @Bean
    public JobRepository jobRepository(
            DataSource dataSource, PlatformTransactionManager transactionManager, @Value("${jdbc.url:}") String jdbcUrl)
                    throws Exception {
        String jdbcType = Validate.notBlank(DBUtils.getDriverType(jdbcUrl), "Cannot determine driver type of JDBC URL=%s", jdbcUrl);
        String dbType = Validate.notBlank(JDBC2DBTYPE.get(jdbcType), "Unsupported JDBC driver type (%s) for URL=%s", jdbcType, jdbcUrl);
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDatabaseType(dbType);
        factory.setTransactionManager(transactionManager);
        factory.setDataSource(dataSource);
        factory.setIsolationLevelForCreate(String.valueOf(TransactionDefinition.ISOLATION_DEFAULT));
        return factory.getObject();
    }

    /**
     * Creates a <U>dedicated</U> task executor to be used by the
     * {@link #jobLauncher(JobRepository, TaskExecutor) jobLauncher}
     *
     * @return The created thread-pool executor
     */
    @Bean(name = JOB_EXECUTOR_BEAN)
    public TaskExecutor jobExecutor() {
        return createThreadPoolTaskExecutor(JOB_EXECUTOR_BEAN, 1000, 1, 1);
    }

    @Bean(name = STEP_EXECUTOR_BEAN)
    public ThreadPoolTaskExecutor stepExecuter(
            @Value("${" + STEP_EXECUTER_QUEUE_CAPACITY_PROP + ":" + DEFAULT_STEP_EXECUTER_QUEUE_CAPACITY + "}") int queueCapacity) {
        int numberOfCPUs = Runtime.getRuntime().availableProcessors();
        return createThreadPoolTaskExecutor(STEP_EXECUTOR_BEAN, queueCapacity, numberOfCPUs, numberOfCPUs);
    }

    @Bean(name = JOB_LAUNCHER)  // make sure matches expected XML value
    public JobLauncher jobLauncher(JobRepository repo, @Named(JOB_EXECUTOR_BEAN) TaskExecutor executor) {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(repo);
        jobLauncher.setTaskExecutor(executor);
        return jobLauncher;
    }

    @Bean
    public JobRegistry jobRegistry() {
        return new MapJobRegistry();
    }

    @Bean
    public JobExplorerFactoryBean jobExplorer(DataSource dataSource) {
        JobExplorerFactoryBean factoryBean = new JobExplorerFactoryBean();
        factoryBean.setDataSource(dataSource);
        return factoryBean;
    }

    @Bean
    public JobOperator jobOperator(JobRegistry jobRegistry, JobRepository jobRepository, JobLauncher jobLauncher, JobExplorer jobExplorer) throws Exception {
        SimpleJobOperator simpleJobOperator = new SimpleJobOperator();
        simpleJobOperator.setJobRepository(jobRepository);
        simpleJobOperator.setJobLauncher(jobLauncher);
        simpleJobOperator.setJobRegistry(jobRegistry);
        simpleJobOperator.setJobExplorer(jobExplorer);
        return simpleJobOperator;
    }

    @Bean
    public JobBuilderFactory jobBuilderFactory(JobRepository jobRepository) {
        return new JobBuilderFactory(jobRepository);
    }

    @Bean
    public StepBuilderFactory stepBuilderFactory(JobRepository jobRepository, PlatformTransactionManager transactionManager) {
        return new StepBuilderFactory(jobRepository, transactionManager);
    }

    protected ThreadPoolTaskExecutor createThreadPoolTaskExecutor(
            String beanName, int queueCapacity, int poolCapacity, int corePoolSize) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setDaemon(true);
        executor.setThreadNamePrefix(beanName + "-");
        executor.setQueueCapacity(queueCapacity);
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(poolCapacity);
        executor.setAllowCoreThreadTimeOut(true);
        logger.info("createThreadPoolTaskExecutor(name={}, queueCapacity={}, poolCapacity={}, corePoolSize={})",
            beanName, queueCapacity, poolCapacity, corePoolSize);
        return executor;
    }
}

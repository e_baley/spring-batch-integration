package com.cb4.demo.spring.batch.integration.core;

public enum CommonAppConfig {
    ;

    public static final String CONFIG_PROP_PREFIX = "demo.spring.batch.integration";
}

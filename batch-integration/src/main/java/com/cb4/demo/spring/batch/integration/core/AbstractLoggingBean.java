package com.cb4.demo.spring.batch.integration.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractLoggingBean {
    protected final Logger logger;

    protected AbstractLoggingBean() {
        logger = LoggerFactory.getLogger(getClass());
    }
}

package com.cb4.demo.spring.batch.integration.jobs;

import java.util.Arrays;

import javax.management.MBeanServer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.integration.support.utils.IntegrationUtils;
import org.springframework.jmx.support.MBeanServerFactoryBean;
import org.springframework.util.Assert;

import com.cb4.demo.spring.batch.integration.core.AbstractLoggingBean;

@Configuration
@EnableMBeanExport(defaultDomain = CoreConfigBean.MBEAN_DOMAIN_PREFIX)
public class CoreConfigBean extends AbstractLoggingBean {
    /** Common prefix of the exported beans JMX domain for non-application specific beans */
    public static final String MBEAN_DOMAIN_PREFIX = "com.cb4.demo.spring.batch.integration";

    public static final String MBEAN_SERVER = "mbeanServer";

    public CoreConfigBean() {
        super();
    }

    /*
     * Make sure this conversion service is used by Spring integration as well
     *
     * see http://docs.spring.io/spring-integration/docs/4.1.1.RELEASE/reference/html/messaging-endpoints-chapter.html#payload-type-conversion
     */
    @Bean(name = { ConfigurableApplicationContext.CONVERSION_SERVICE_BEAN_NAME, IntegrationUtils.INTEGRATION_CONVERSION_SERVICE_BEAN_NAME })
    public FormattingConversionServiceFactoryBean conversionService() {
        return new FormattingConversionServiceFactoryBean();
    }

    @Bean(MBEAN_SERVER)
    public MBeanServer mbeanServer() throws Exception {
        EnableMBeanExport exportConfig = AnnotationUtils.findAnnotation(getClass(), EnableMBeanExport.class);
        Assert.notNull(exportConfig, "Configuration bean class not annotated with " + EnableMBeanExport.class.getSimpleName());
        String defaultDomain = Validate.notBlank(exportConfig.defaultDomain(), "No default domain configured", ArrayUtils.EMPTY_OBJECT_ARRAY);
        MBeanServerFactoryBean factory = new MBeanServerFactoryBean();
        factory.setLocateExistingServerIfPossible(true);
        factory.setDefaultDomain(defaultDomain);
        factory.afterPropertiesSet();

        MBeanServer server = factory.getObject();
        logger.info("mbeanServer(defaultDomain={}) domains: {}", server.getDefaultDomain(), Arrays.toString(server.getDomains()));
        return server;
    }
}

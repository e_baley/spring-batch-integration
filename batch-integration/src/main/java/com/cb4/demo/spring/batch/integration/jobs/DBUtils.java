package com.cb4.demo.spring.batch.integration.jobs;

import org.apache.commons.lang3.StringUtils;

public enum DBUtils {
    ;
    /**
     * Standard prefix of JDBC URLs
     */
    public static final String JDBC_URL_PREFIX = "jdbc:";

    /**
     * Extracts the driver type part from a standard JDBC URL: {@code jdbc:some-driver:extra}
     *
     * @param url The JDBC URL - ignored if {@code null}/empty or malformed (e.g., not starting
     * with the {@link #JDBC_URL_PREFIX} or missing driver)
     * @return The resolved driver type or {@code null} if none could be extracted
     */
    public static String getDriverType(String url) {
        if (StringUtils.isBlank(url)) {
            return null;
        }

        // make sure this is a JDBC URL
        if ((url.length() <= JDBC_URL_PREFIX.length()) || (!StringUtils.startsWithIgnoreCase(url, JDBC_URL_PREFIX))) {
            return null;
        }

        // extract the driver type part
        int pos = url.indexOf(':', JDBC_URL_PREFIX.length());
        if (pos <= JDBC_URL_PREFIX.length()) {
            return null; // malformed or missing driver type
        }

        return url.substring(JDBC_URL_PREFIX.length(), pos);
    }
}

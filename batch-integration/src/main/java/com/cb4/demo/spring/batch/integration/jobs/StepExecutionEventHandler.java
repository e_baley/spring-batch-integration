package com.cb4.demo.spring.batch.integration.jobs;

import org.apache.commons.lang3.Validate;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.GenericMessage;

import com.cb4.demo.spring.batch.integration.core.TypedPayloadLoggingHandler;

public class StepExecutionEventHandler extends TypedPayloadLoggingHandler<StepExecution> {
    public StepExecutionEventHandler(String level) {
        super(StepExecution.class, level);
    }

    public StepExecutionEventHandler(Level level) {
        super(StepExecution.class, level);
    }

    @Override
    protected void handleMessageInternal(StepExecution stepExecution, MessageHeaders headers) throws Exception {
        logger.info("handleMessageInternal(headers=" + headers + "): " + stepExecution);

        // see https://github.com/spring-projects/spring-integration/issues/2161
        Object replyChannel = headers.get(MessageHeaders.REPLY_CHANNEL);
        if (replyChannel == null) {
            return; // for beforeStep there is no reply
        }

        Validate.isInstanceOf(MessageChannel.class, replyChannel, "Invalid reply channel type: %s", replyChannel);
        ExitStatus exitStatus = stepExecution.getExitStatus();
        logger.info("handleMessageInternal(headers=" + headers + ") reply=" + exitStatus);
        ((MessageChannel) replyChannel).send(new GenericMessage<>(exitStatus));
    }
}
